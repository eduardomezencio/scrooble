"""Artificial Intelligence to play scroobble."""

import logging
import os
import pickle
import random
import sys
from itertools import chain
from typing import List, Iterable

import data
import helper
import validation
from board import Board
from play import Play
from player_base import PlayerBase
from prefix_tree import PrefixTree


class AI(PlayerBase):
    """Artificial Intelligence to play scroobble."""
    def __init__(self, player=None, force_build_tree=False):
        super().__init__(player)
        self.bag_len: int
        self.board: Board
        self.tiles: List[str]
        self.tree: PrefixTree
        self.turn: int
        self._load_tree(force_build_tree)

    def play(self):
        def find_plays(fast: bool) -> Iterable[Play]:
            return chain.from_iterable(
                (self._get_best_play(
                    i, dir, self._find_possible_words(i, dir, fast))
                 for i in range(Board.SIZE)) for dir in ('a', 'd'))

        self._load_state()
        best_play = None
        if self.tiles:
            best_play, _ = max(find_plays(fast=True), key=lambda ps: ps[1])
            if best_play is None:
                best_play, _ = next(find_plays(fast=False), (None, None))
        if best_play is None:
            best_play = self._change_or_pass()

        logging.debug('Playing %s as player %d', best_play, self.turn + 1)
        return str(best_play)

    def _load_tree(self, force=False):
        if not force and os.path.isfile('tree'):
            with open('tree', 'rb') as file:
                self.tree = pickle.load(file)
        else:
            self.tree = PrefixTree()
            with open('tree', 'wb') as file:
                old_recursion_limit = sys.getrecursionlimit()
                sys.setrecursionlimit(6000)
                pickle.dump(self.tree, file)
                sys.setrecursionlimit(old_recursion_limit)

    def _load_state(self):
        with open('log') as file:
            self.turn = int(file.readline()) - 1
            log = [Play(line) for line in file]
        self.board = Board()
        for play in log:
            if play.type == Play.PLAY:
                self.board.write_play(play)
            self.turn = (self.turn + 1) % 2
        self.bag_len = len(helper.read_char_list('bag'))
        self.tiles = helper.read_char_list(f'p{self.turn + 1}')
        logging.debug('State loaded. bag_len = %d, tiles = %s',
                      self.bag_len, self.tiles)

    def _find_possible_words(self, index: int, direction: str, fast=True):
        if not self.board.check_if_connected(index, direction):
            return
        tiles = self.tiles.copy()
        tiles.extend(self.board.get_letters(index, direction))

        possible_words = set()
        for possible in self.tree.possible_words(tiles, fast):
            if self._interrupt:
                return
            if possible not in possible_words:
                possible_words.add(possible)
                yield possible

    def _get_best_play(self, index: int, direction: str, words):
        best_play, best_score = None, 0
        for word in words:
            if direction == 'a':
                row, col, drow, dcol = index, 0, 0, 1
            else:
                row, col, drow, dcol = 0, index, 1, 0
            for _ in range(Board.SIZE - len(word) + 1):
                play = Play(f'{hex(row)[2]}{hex(col)[2]}{direction} {word}')
                try:
                    validation.validate_play(play, self.board, self.tiles)
                    score = self._calculate_play_score(play)
                    if score > best_score:
                        best_play, best_score = play, score
                except validation.ValidationException:
                    pass
                row += drow
                col += dcol
        return best_play, best_score

    def _calculate_play_score(self, play: Play):
        words = []

        def add_word(word, mult_list):
            value = helper.calculate_value(word, mult_list)
            words.append((word, value))

        word_mult = []
        for row, col, char in play.letters():
            if self.board.read(row, col) == ' ':
                letter_mult = []
                wildcard = (row, col) in play.wildcards
                mult = data.MULTIPLIERS.get((row, col), None)
                if mult and not (mult[0] == 'l' and wildcard):
                    letter_mult.append((mult, char))
                word_mult.extend(letter_mult)

                secondary_word = self.board.get_word_in_coord(
                    row, col, helper.flip_direction(play.direction))
                if secondary_word:
                    add_word(secondary_word, letter_mult)
        add_word(play.word, word_mult)
        secondary_word = self.board.get_word_in_coord(play.row, play.column,
                                                      play.direction)
        if secondary_word and secondary_word.lower() != play.word.lower():
            add_word(secondary_word, word_mult)

        return sum(w[1] for w in words)

    def _change_or_pass(self) -> Play:
        logging.debug('Change or pass. bag_len = %d', self.bag_len)
        if self.bag_len < 1:
            return Play('pass')

        change_max = min(len(self.tiles), self.bag_len)
        change_tiles = random.sample(self.tiles,
                                     random.randrange(change_max) + 1)
        return Play(f'change {"".join(change_tiles)}')


def main():
    """Starts the AI using command line arguments. -f forces the prefix tree to
    be rebuilt.
    """
    player = PlayerBase.read_player_from_args()
    force_build_tree = '-f' in sys.argv

    with AI(player, force_build_tree) as intel:
        intel.start()


if __name__ == "__main__":
    main()
