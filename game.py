"""Scroobble game class and main script."""

import io
import os
import sys
from itertools import chain, repeat
from random import randrange, seed, shuffle
from threading import Thread
from time import sleep
from typing import List, Tuple

import data
import helper
import validation
from board import Board
from play import Play, InvalidPlayException


class Game:
    """Scroobble game."""
    TILES_NUM = 7

    def __init__(self, newgame=False):
        self.bag: List[str]
        self.board: Board
        self.finished: bool
        self.first: int
        self.io_command: List[io.TextIOWrapper]
        self.io_notify: List[io.TextIOWrapper]
        self.log: List[Play]
        self.passes: int
        self.player_tiles: List[List[str]]
        self.player_words: List[List[Tuple[str, int]]]
        self.turn: int

        if newgame:
            self.new_game()
            self.save_state()
        else:
            self.load_state()

    @property
    def current_player_tiles(self):
        """The current turn's player tiles."""
        return self.player_tiles[self.turn]

    @property
    def current_player_words(self):
        """The current turn's player words."""
        return self.player_words[self.turn]

    def char(self, row, col, patch=None):
        """Return the character in the given coordinats, with the given patch
        applied.
        """
        return self.board.read(row, col, patch)

    def score(self, player=None):
        """Return the score of a player. If no player is passed, return the
        score of current turn's player.
        """
        if player is None:
            player = self.turn
        return sum(w[1] for w in self.player_words[player])

    def new_game(self):
        """Start a new game."""
        self.bag = []
        self.board = Board()
        self.finished = False
        self.first = randrange(2)
        self.log = []
        self.passes = 0
        self.player_tiles = [[], []]
        self.player_words = [[], []]
        self.turn = self.first

        for char, (count, _) in data.LETTERS.items():
            self.bag.extend(repeat(char, count))
        shuffle(self.bag)

        for _ in range(Game.TILES_NUM):
            self.player_tiles[0].append(self.bag.pop())
            self.player_tiles[1].append(self.bag.pop())

    def load_state(self):
        """Load the game state from disk."""
        with open('log', 'r') as file:
            self.first = int(file.readline()) - 1
            self.log = [Play(line) for line in file]

        self.bag = helper.read_char_list('bag')
        self.player_tiles = [helper.read_char_list('p1'),
                             helper.read_char_list('p2')]
        self.board = Board()
        self.finished = False
        self.passes = 0
        self.player_words = [[], []]
        self.turn = self.first
        for play in self.log:
            self.play(play, fake=True)

    def save_state(self):
        """Save the game state to disk."""
        with open('log', 'w') as log_file:
            log_file.write(f'{self.first + 1}\n')
            log_file.write('\n'.join(str(play) for play in self.log))

        helper.write_char_list('bag', self.bag)
        helper.write_char_list('p1', self.player_tiles[0])
        helper.write_char_list('p2', self.player_tiles[1])

    def get_command(self) -> str:
        """Read a command from the current turn's player."""
        self._notify_turn()
        command = self.io_command[self.turn].readline().strip('\n\t ')
        return command

    def run_command(self, command: str):
        """Run a command for the current turn."""
        def print_():
            print(self)

        def words():
            print(self.words_str())

        def play_():
            self.play(command)
            self.save_state()

        {'print': print_, 'words': words}.get(command, play_)()

    def words_str(self, player: int = None):
        """Build a string with the words formed by each player with the score
        for each word.
        """
        if player is None:
            return '\n'.join(self.words_str(i) for i in (0, 1))
        try:
            words = ', '.join('{}({})'.format(*w)
                              for w in self.player_words[player])
            return f'Player {player + 1}: {words}'
        except IndexError:
            return ''

    def play(self, play, fake=False):
        """Execute a play."""
        if isinstance(play, str):
            play = Play(play)

        if not fake:
            self.validate_play(play)
            self.log.append(play)

        if play.type == Play.PASS:
            self._play_pass()
        elif play.type == Play.CHANGE:
            self._play_change(play, fake)
        elif play.type == Play.PLAY:
            self._play_play(play, fake)

        self.turn = (self.turn + 1) % 2

    def _play_pass(self):
        self.passes += 1
        if self.passes >= 4:
            self.finished = True

    def _play_change(self, play: Play, fake=False):
        self.passes = 0
        if not fake:
            for char in play.word:
                tiles = self.current_player_tiles
                tiles[tiles.index(char)] = self.bag.pop()
            for char in play.word:
                self.bag.append(char)
            shuffle(self.bag)

    def _play_play(self, play: Play, fake=False):

        def add_word(word, mult_list):
            value = helper.calculate_value(word, mult_list)
            self.current_player_words.append((word, value))

        self.passes = 0
        word_mult = []
        spend = []

        for row, col, char in play.letters():
            if self.char(row, col) == ' ':
                letter_mult = []
                wildcard = (row, col) in play.wildcards
                if wildcard:
                    self.board.write(row, col, char.upper())
                    spend.append('_')
                else:
                    self.board.write(row, col, char)
                    spend.append(char)

                mult = data.MULTIPLIERS.get((row, col), None)
                if mult and not (mult[0] == 'l' and wildcard):
                    letter_mult.append((mult, char))

                word_mult.extend(letter_mult)

                secondary_word = self.board.get_word_in_coord(
                    row, col, helper.flip_direction(play.direction))
                if secondary_word:
                    add_word(secondary_word, letter_mult)
        add_word(play.word, word_mult)
        secondary_word = self.board.get_word_in_coord(play.row, play.column,
                                                      play.direction)
        if secondary_word and secondary_word.lower() != play.word.lower():
            add_word(secondary_word, word_mult)

        if not fake:
            self.spend_tiles(spend)

    def spend_tiles(self, tiles: List[str], player: int = None):
        """Remove tiles from the player and replace them with new tiles taken
        from the bag, if the bag is not empty.
        """
        if player is None:
            player = self.turn
        for char in tiles:
            index = self.player_tiles[player].index(char)
            if self.bag:
                self.player_tiles[player][index] = self.bag.pop()
            else:
                del self.player_tiles[player][index]

    def validate_play(self, play: Play):
        """Validate a play. Raise a ValidationException if there's a problem.
        """
        if play.type == Play.CHANGE:
            self._validate_change(play)
        elif play.type == Play.PLAY:
            self._validate_play(play)

    def _validate_change(self, play: Play):
        validation.change_bag_size_validation(play, self.bag)
        validation.change_player_tiles_validation(play,
                                                  self.current_player_tiles)

    def _validate_play(self, play: Play):
        validation.validate_play(play, self.board, self.current_player_tiles)

    def _create_io(self):
        def wait_for(player: int):
            print(f'Waiting for player {player + 1}...\n', end='')
            self.io_command[player] = open(f'command{player + 1}', 'r')
            self.io_notify[player] = open(f'notify{player + 1}', 'w')
            print(f'Player {player + 1} connected.\n', end='')

        self.io_command = [None, None]
        self.io_notify = [None, None]
        for name in ('command1', 'command2', 'notify1', 'notify2'):
            if os.path.exists(name):
                os.remove(name)
            os.mkfifo(name)
        threads = [Thread(None, wait_for, f'wait_for_{i + 1}', args=(i,))
                   for i in (0, 1)]
        for thread in threads:
            thread.start()
            sleep(0.1)
        for thread in threads:
            thread.join()

    def _destroy_io(self):
        for file in chain(self.io_command, self.io_notify):
            try:
                file.close()
            except BrokenPipeError:
                pass
        for name in ('command1', 'command2', 'notify1', 'notify2'):
            if os.path.exists(name):
                os.remove(name)

    def _notify_turn(self):
        self.io_notify[self.turn].write(f'{self.turn}\n')
        self.io_notify[self.turn].flush()

    def __enter__(self):
        self._create_io()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self._destroy_io()

    def __str__(self):
        players = '\n'.join(f'{"->" if self.turn == p else "  "} '
                            f'Player {p + 1}: '
                            f'{"".join(self.player_tiles[p]).upper()}  '
                            f'Score: {self.score(p)}' for p in range(2))
        bag = f'Bag: {len(self.bag)}'

        return '\n'.join((str(self.board), players, bag))


def main():
    """Run scroobble with command line args. -s followed by a number sets a
    seed for the random number generator. -n begins a new game. The default is
    to continue the previous game.
    """
    if '-s' in sys.argv:
        seed_index = sys.argv.index('-s')
        seed(int(sys.argv[seed_index + 1]))

    last_turn = -1
    with Game('-n' in sys.argv) as game:
        try:
            while not game.finished:
                if game.turn != last_turn:
                    print(game)
                    last_turn = game.turn
                try:
                    command = game.get_command()
                    game.run_command(command)
                except (validation.ValidationException,
                        InvalidPlayException) as exception:
                    print(f'Invalid input: {exception}')
        except (BrokenPipeError, KeyboardInterrupt):
            pass


if __name__ == "__main__":
    main()
