"""A play in scroobble."""

from typing import List

import board
from aliases import Wildcards


class InvalidPlayException(Exception):
    """Invalid play error."""
    pass


class Play:
    """A playe in scroobble."""
    PLAY = 1
    PASS = 2
    CHANGE = 3

    def __init__(self, command: str, keep_case=False):
        self.row: int
        self.column: int
        self.direction: str
        self.word: str
        self.type: int
        self.wildcards: Wildcards
        self.wildcards = set()
        input_list = command.lower().split()
        if not input_list:
            raise InvalidPlayException('No command given.')
        if input_list[0] == 'pass':
            self._init_pass()
        elif input_list[0] == 'change':
            self._init_change(input_list)
        else:
            self._init_play(input_list, keep_case)

    def _init_play(self, command: List[str], keep_case):
        self.type = Play.PLAY
        try:
            self.row = int(command[0][0], 16)
            self.column = int(command[0][1], 16)
            self.direction = command[0][2]
            self.word = command[1] if keep_case else command[1].lower()
        except (IndexError, ValueError):
            raise InvalidPlayException('Invalid position.')
        if any((self.direction not in ('a', 'd'),
                self.row >= board.Board.SIZE,
                self.column >= board.Board.SIZE)):
            raise InvalidPlayException('Invalid position.')

    def _init_pass(self):
        self.type = Play.PASS

    def _init_change(self, command: List[str]):
        self.type = Play.CHANGE
        self.word = command[1].lower()

    def letters(self):
        """Generator returning the word played, letter by letter, in tuples
        (row, column, letter).
        """
        if self.type != Play.PLAY:
            return

        row, col = self.row, self.column
        if self.direction == 'a':
            drow, dcol = 0, 1
        else:
            drow, dcol = 1, 0

        for char in self.word:
            yield (row, col, char)
            row += drow
            col += dcol

    def patch(self):
        """Generates a patch to be used by the Board. It's a dictionary mapping
        (row, column) tuples to each letter in the word."""
        return {(row, col): char for row, col, char in self.letters()}

    def set_wildcards(self, wildcards: Wildcards):
        """Sets the wildcards used in this play."""
        self.wildcards = wildcards
        self.word = ''.join(
            self.word[i].lower() if c[:2] not in wildcards
            else self.word[i].upper()
            for i, c in enumerate(self.letters()))

    def __str__(self):
        if self.type == Play.CHANGE:
            return f'change {self.word}'
        if self.type == Play.PASS:
            return 'pass'
        return (f'{hex(self.row)[2:]}{hex(self.column)[2:]}'
                f'{self.direction} {self.word}')
