"""The board for scroobble."""

from copy import deepcopy
from itertools import chain, repeat
from typing import Generator, List, Tuple

import data
import play
from aliases import NeededTiles, Wildcards


class Board:
    """The board for scroobble."""
    SIZE = 15
    CENTER = 7
    _HEADER = ' 0123456789ABCDE '

    def __init__(self):
        self._data: List[List[str]]
        self._data = [[' ' for i in range(Board.SIZE)]
                      for j in range(Board.SIZE)]

    def read(self, row: int, col: int, patch: dict = None) -> str:
        """Returns character in the given coordinates, considering patches
        applied to the board.
        """
        if not (0 <= row < Board.SIZE and 0 <= col < Board.SIZE):
            return ''
        if patch:
            char = patch.get((row, col), None)
            if char:
                return char
        return self._data[row][col]

    def write(self, row: int, col: int, char: str):
        """Writes a character to the board in the given coordinates."""
        self._data[row][col] = char[0]

    def write_play(self, play_: play.Play):
        """Writes word from a play to the board."""
        for row, col, char in play_.letters():
            self._data[row][col] = char

    def check_if_fits(self, play_: play.Play
                      ) -> Tuple[bool, NeededTiles, Wildcards]:
        """Checks if the word in a play fits in the board."""
        connected = False
        needed_tiles = {}
        wildcards = set()
        for row, col, char in play_.letters():
            board_char = self.read(row, col)
            if board_char == ' ':
                needed_tiles.setdefault(char, []).append((row, col))
            elif not board_char or board_char.lower() != char:
                return False, needed_tiles, wildcards
            elif board_char.isupper():
                wildcards.add((row, col))
            if not connected:
                connected = (row == col == 7 or
                             any(self.read(r, c) not in ('', ' ') for r, c in [
                                 (row, col), (row, col + 1), (row + 1, col),
                                 (row, col - 1), (row - 1, col)]))
        return connected, needed_tiles, wildcards

    def check_if_connected(self, index: int, direction: str
                           ) -> Generator[str, None, None]:
        """Checks if a given line or column is connected to any letter on the
        board.
        """
        if direction == 'a':
            return self.check_if_line_connected(index)
        return self.check_if_column_connected(index)

    def check_if_line_connected(self, index: int):
        """Checks if a given line is connected to any letter on the board."""
        if index == Board.CENTER:
            return True
        try:
            indexes = [index]
            if index >= 1:
                indexes.append(index - 1)
            if index < Board.SIZE - 1:
                indexes.append(index + 1)
            next(chain.from_iterable(self.get_line_letters(i)
                                     for i in indexes))
            return True
        except StopIteration:
            return False

    def check_if_column_connected(self, index: int):
        """Checks if a given column is connected to any letter on the board."""
        if index == Board.CENTER:
            return True
        try:
            indexes = [index]
            if index >= 1:
                indexes.append(index - 1)
            if index < Board.SIZE - 1:
                indexes.append(index + 1)
            next(chain(self.get_column_letters(i) for i in indexes))
            return True
        except StopIteration:
            return False

    def get_letters(self, index: int, direction: str
                    ) -> Generator[str, None, None]:
        """Returns all letters in a given line or column."""
        if direction == 'a':
            yield from self.get_line_letters(index)
            return
        yield from self.get_column_letters(index)

    def get_line_letters(self, index: int) -> Generator[str, None, None]:
        """Returns all letters in a given line."""
        yield from (c for c in self._data[index] if c != ' ')

    def get_column_letters(self, index: int) -> Generator[str, None, None]:
        """Returns all letters in a given column."""
        yield from (r[index] for r in self._data if r[index] != ' ')

    def get_words(self, index: int, direction: str, patch=None, min_len=2
                  ) -> Generator[str, None, None]:
        """Returns all words in a given line or column."""
        direction = direction.lower()
        if direction == 'a':
            yield from self.get_line_words(index, patch, min_len)
            return
        yield from self.get_column_words(index, patch, min_len)

    def get_line_words(self, index, patch=None, min_len=2
                       ) -> Generator[str, None, None]:
        """Returns all words in a given line."""
        row = [self.read(r, c, patch) for r, c in zip(repeat(index),
                                                      range(Board.SIZE))]
        words = ''.join(row).split()
        yield from (w.lower() for w in words if len(w) >= min_len)

    def get_column_words(self, index, patch=None, min_len=2
                         ) -> Generator[str, None, None]:
        """Returns all words in a given column."""
        col = [self.read(r, c, patch) for r, c in zip(range(Board.SIZE),
                                                      repeat(index))]
        words = ''.join(col).split()
        yield from (w.lower() for w in words if len(w) >= min_len)

    def get_all_words(self, patch=None, min_len=2
                      ) -> Generator[str, None, None]:
        """Returns all words on the board."""
        for i in range(Board.SIZE):
            yield from self.get_line_words(i, patch, min_len)
            yield from self.get_column_words(i, patch, min_len)

    def get_word_in_coord(self, row, col, direction) -> str:
        """Returns the word in the given coordinates and direction, if any."""
        if self.read(row, col) in ('', ' '):
            return ''

        if direction == 'a':
            drow, dcol = 0, 1
        elif direction == 'd':
            drow, dcol = 1, 0
        else:
            return ''

        while self.read(row - drow, col - dcol) not in ('', ' '):
            row -= drow
            col -= dcol

        word = [self.read(row, col)]
        while self.read(row + drow, col + dcol) not in ('', ' '):
            row += drow
            col += dcol
            word.append(self.read(row, col))

        if len(word) > 1:
            return ''.join(word)
        return ''

    def __str__(self):
        board = deepcopy(self._data)
        for coord, mult in data.MULTIPLIERS.items():
            if board[coord[0]][coord[1]] == ' ':
                board[coord[0]][coord[1]] = data.MULTIPLIER_CHARS[mult]
        board.insert(0, [c for c in Board._HEADER[1:-1]])
        board.append([c for c in Board._HEADER[1:-1]])
        for i, char in enumerate(Board._HEADER):
            board[i].insert(0, char)
            board[i].append(char)
        return '\n'.join(' '.join(row) for row in board)
