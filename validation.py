"""Play validation functions."""

from itertools import islice
from typing import List, Tuple

import data
from aliases import NeededTiles, Wildcards
from board import Board
from play import Play


class ValidationException(Exception):
    """Play validation error."""
    pass


def play_fits_board_validation(play: Play, board: Board) -> Tuple[NeededTiles,
                                                                  Wildcards]:
    """Raise ValidationException if the play does not fit on the board. Will
    check if tiles can be or already are placed in the correct coordinates on
    the board and if the word is connected with other words.
    """
    fits, needed_tiles, wildcards = board.check_if_fits(play)
    if not (fits and needed_tiles):
        raise ValidationException('Word not connected to other game tiles.')
    return needed_tiles, wildcards


def play_player_tiles_validation(tiles: List[str], needed_tiles: NeededTiles):
    """Raise ValidationException if the player does not have the needed tiles.
    """
    wildcards = tiles.count('_')
    wildcards_set = set()
    for char, coords in needed_tiles.items():
        count = len(coords)
        player_count = tiles.count(char)
        if player_count < count:
            if player_count + wildcards >= count:
                used = count - player_count
                wildcards -= used
                wildcards_set.update(islice(coords, used))
            else:
                raise ValidationException(
                    f"You don't have enough '{char.upper()}'s.")
    return wildcards_set


def play_words_validation(play: Play, board: Board):
    """Raie ValidationException if play forms a word that does not exist."""
    for word in board.get_all_words(play.patch()):
        if word not in data.DICTIONARY:
            raise ValidationException(f'Word "{word}" does not exist.')


def validate_play(play: Play, board: Board, tiles: List[str]):
    """Validate a play. Other validations are called and the wildcards needed
    by the play are updated.
    """
    needed_tiles, wildcards = play_fits_board_validation(play, board)
    wildcards.update(play_player_tiles_validation(tiles, needed_tiles))
    play.set_wildcards(wildcards)
    play_words_validation(play, board)


def change_bag_size_validation(play: Play, bag: List[str]):
    """Raise ValidationException if bag does not have enough tiles for a
    change.
    """
    if len(bag) < len(play.word):
        raise ValidationException(
            f'Bag has only {len(bag)} tiles.')


def change_player_tiles_validation(play: Play, tiles: List[str]):
    """Raise ValidationException if the player does not have the tiles to be
    changed.
    """
    char_count = {}
    for char in play.word:
        char_count.setdefault(char, 0)
        char_count[char] += 1
    for char, count in char_count.items():
        if tiles.count(char) < count:
            raise ValidationException(
                f"You don't have enough '{char.upper()}'s.")
