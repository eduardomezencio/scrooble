"""Convenience types for annotations."""

from typing import Dict, List, Set, Tuple

Coord = Tuple[int, int]
NeededTiles = Dict[str, List[Coord]]
Wildcards = Set[Coord]
