from player_base import PlayerBase


class Player(PlayerBase):
    def play(self):
        return input()


def main():
    player_num = PlayerBase.read_player_from_args()

    with Player(player_num) as player:
        player.start()


if __name__ == "__main__":
    main()
