"""Helper functions for scroobble."""

from itertools import islice
from typing import List, Tuple

import data


def read_char_list(filename: str):
    """Read characters in a file to a list of characters."""
    with open(filename, 'r') as file:
        return [c for c in file.readline().lower() if c != '\n']


def write_char_list(filename: str, char_list: List[str]):
    """Write a list of characters to a file."""
    with open(filename, 'w') as file:
        file.write(''.join(char_list).lower())


def flip_direction(direction: str) -> str:
    """Toggle direction between 'a'(horizontal) and 'd'(vertical)."""
    if direction.lower() == 'a':
        return 'd'
    return 'a'


def calculate_value(word: str, mult_list: List[Tuple[Tuple[str, int], str]]):
    """Calculate the point value of a word, accounting for multipliers."""
    value = sum(data.LETTERS.get(l, (0, 0))[1] for l in word)
    word_mult = 1
    for mult, letter in mult_list:
        if mult[0] == 'w':
            word_mult *= mult[1]
        else:
            value += data.LETTERS.get(letter, (0, 0))[1] * (mult[1] - 1)
    value *= word_mult
    return value


def get_sub_keys(key: List[str]):
    """Get each key formed by removing one element from the given key, except
    for the last one.
    """
    last_char = ''
    for i, char in enumerate(islice(key, len(key)-1)):
        if char == last_char:
            continue
        last_char = char
        yield [c for n, c in enumerate(key) if n != i]
