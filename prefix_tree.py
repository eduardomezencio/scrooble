"""Prefix tree implementation."""

import logging
from collections import deque
from itertools import chain
from typing import Iterable, List, Set

import helper


class Node:
    """Prefix tree node."""
    def __init__(self, char: str, parent: 'Node' = None):
        self.char = char
        self.children = []
        self.links = []
        self.words = set()
        self.parent = parent

    def child(self, char: str) -> 'Node':
        """Get the child of the node with given character or None."""
        index = 26 if char == 'ç' else ord(char) - 97
        if index >= len(self.children):
            return None
        return self.children[index]

    def add_child(self, char: str) -> 'Node':
        """Add a child with given character to the node."""
        node = Node(char, self)
        index = 26 if char == 'ç' else ord(char) - 97
        if index >= len(self.children):
            self.children.extend(None for _ in
                                 range(index - len(self.children) + 1))
        self.children[index] = node
        return node

    def add_word(self, index: int, word: str, key: List[str]):
        """Add a word to the tree recursively."""
        if index >= len(key):
            self.words.add(word)
            return

        node = self.child(key[index])
        if not node:
            node = self.add_child(key[index])

        node.add_word(index + 1, word, key)

    def visit(self, visited: set, func=None):
        """Travel nodes up the tree until a node already in visited is reached.
        If a function is given, yields from what this function returns for each
        node.
        """
        node = self
        while node not in visited:
            visited.add(node)
            if func:
                yield from func(node)
            node = node.parent

    def __repr__(self):
        return f'{self.char}: {self.words}'


class PrefixTree:
    """Prefix tree for searching possible words formed with a collection of
    letters.
    """
    def __init__(self, file_name='dict'):
        self.root: Node
        self.root = Node('Root')
        self._build(file_name)

    def possible_words(self, letters: List[str], fast=True):
        """Get all possible words formed with the given collection of letters.
        If fast is False, do a more complete search.
        """
        if fast:
            yield from self._possible_words_fast(letters)
        else:
            yield from self._possible_words_all(letters)

    def _possible_words_fast(self, letters: List[str]):
        letters = sorted(filter(lambda c: c != '_', letters))
        visited, words = set(), list()
        node = self._find_node(letters, visited, words, False, True)
        yield from chain.from_iterable(words)
        links = deque(node.links)
        while links:
            node = links.popleft()
            if node not in visited:
                links.extend(node.links)
            while node not in visited:
                visited.add(node)
                yield from node.words
                node = node.parent

    def _possible_words_all(self, letters: List[str]):
        logging.info('searching all words')
        letters = sorted(letters)
        visited, words = set(), list()
        node = self._find_node(letters, visited, words, False, True)
        yield from chain.from_iterable(words)
        sub_keys = deque(helper.get_sub_keys(letters))
        while sub_keys:
            sub_key = sub_keys.popleft()
            node = self._find_node(sub_key)
            if node is None:
                sub_keys.extend(helper.get_sub_keys(sub_key))
            else:
                yield from node.visit(visited, lambda n: n.words)

    def _build(self, file_name):
        with open(file_name) as file:
            for line in file:
                word = line.strip('\n\t ')
                self.root.add_word(0, word, sorted(word))
        for node, key in self._pre_order():
            self._create_links(node, key)

    def _create_links(self, node: Node, key: Iterable[str],
                      visited: Set[Node] = None):
        if visited is None:
            visited = set()
            visited.add(self._find_node(key, visited))
        sub_keys = deque(helper.get_sub_keys(key))
        while sub_keys:
            sub_key = sub_keys.popleft()
            linked_node = self._find_node(sub_key, visited, None, True)
            if linked_node not in visited:
                linked_node.visit(visited)
                node.links.append(linked_node)

    def _find_node(self, key: Iterable[str], visited: Set[Node] = None,
                   words: List[Set[str]] = None,
                   last_visited_if_not_exists=False,
                   create_node_if_not_exists=False):
        node = self.root
        depth = 0
        for char in key:
            child = node.child(char)
            depth += 1
            if not child:
                if last_visited_if_not_exists:
                    return node
                if create_node_if_not_exists:
                    child = node.add_child(char)
                    visited_copy = set() if visited is None else visited.copy()
                    self._create_links(child, key[:depth], visited_copy)
                else:
                    return None
            if visited is not None:
                visited.add(node)
            node = child
            if words is not None:
                words.append(node.words)
        return node

    def _pre_order(self):
        def recurse(node: Node, key: List[str]):
            key.append(node.char)
            yield node, key
            for child in node.children:
                if child:
                    yield from recurse(child, key)
            key.pop()

        key = []
        for child in self.root.children:
            if child:
                yield from recurse(child, key)
