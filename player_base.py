"""Base class for scroobble players."""

import io
import logging
import sys
from abc import ABC, abstractmethod
from itertools import chain
from threading import Thread
from typing import List, Set


class PlayerBase(ABC):
    """Base class for scroobble players, that can connect to scroobble using
    named pipes.
    """
    def __init__(self, player):
        logging.basicConfig(format='%(levelname)s:%(message)s',
                            level=logging.DEBUG)

        self.io_command: List[io.TextIOWrapper]
        self.io_notify: List[io.TextIOWrapper]
        self.player: Set[int]

        if player in (0, 1):
            self.player = {player}
        else:
            self.player = {0, 1}

        self._interrupt = False

    @staticmethod
    def read_player_from_args(arg_index=1):
        """Read player from command line arguments."""
        try:
            player = int(sys.argv[arg_index]) - 1
        except (IndexError, ValueError):
            player = -1
        return player

    @abstractmethod
    def play(self) -> str:
        """Method to perform a play for a single turn. Must be implemented by
        subclasses. Should not be called directly, call start instead.
        """
        pass

    def start(self):
        """Method that waits for messages from the game and calls play when
        necessary.
        """
        def loop(player):
            try:
                while True:
                    turn = self.io_notify[player].readline()
                    if int(turn) != player:
                        continue
                    command = self.play()
                    self.io_command[player].write(f'{command}\n')
                    self.io_command[player].flush()
            except (BrokenPipeError, ValueError):
                pass

        threads = [Thread(None, loop, f'play{i + 1}', args=(i,))
                   for i in self.player]
        for thread in threads:
            thread.start()
        while True:
            try:
                for thread in threads:
                    thread.join()
                break
            except KeyboardInterrupt:
                if self._interrupt:
                    break
                self._interrupt = True

    def __enter__(self):
        self.io_command = [None, None]
        self.io_notify = [None, None]
        for player in self.player:
            self.io_command[player] = open(f'command{player + 1}', 'w')
            self.io_notify[player] = open(f'notify{player + 1}', 'r')
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        for file in chain(self.io_command, self.io_notify):
            try:
                file.close()
            except BrokenPipeError:
                pass
